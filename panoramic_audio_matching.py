import pygame
from pygame.locals import *
import random

import math
import numpy

BOOM = "./wav/boom.wav"
HYENA = "./wav/hyena-laugh_daniel-simion.wav"


class AudioSyncer(object):
    """All the logic for our syncer, sound gen and checking/generation """
    DEFAULT_BASIC_TONE = 440.0
    COARSE_STEP_HZ = 40
    FINE_STEP_HZ = 5

    def __init__(self,
            bits=16,
            duration_seconds=0.3,
            sample_rate=44100,
            max_sample=None,
            basic_tone=None,
            freq_init_min_spread=60,
            freq_final_threshold=20,
            tempered_scale="19"):

        self._basic_tone = basic_tone or self.DEFAULT_BASIC_TONE

        self._bits = bits
        self._duration = duration_seconds  # in seconds
        self._sample_rate = sample_rate
        self._n_samples = int(round(self._duration * self._sample_rate))

        self._max_sample = max_sample or (2**(bits - 1) - 1)

        self._threshold = freq_final_threshold

        self._freq_init_min_spread = freq_init_min_spread
        _frequencies = {
            "12": [self._basic_tone * 2**((i+1)/12.0) for i in range(-1, 12)],
            "19": [self._basic_tone * 2**((i+1)/19.0) for i in range(-1, 19)]
        }
        freq_iterable = _frequencies[tempered_scale]
        f_l, f_r = 0, 0
        while (f_r - f_l) < self._freq_init_min_spread:
            f_l = random.choice([_ for _ in freq_iterable])
            f_r = random.choice([_ for _ in freq_iterable])
        self.frequency_l, self.frequency_r = f_l, f_r

    def prepare_sound(self):
        buf = numpy.zeros((self._n_samples, 2), dtype=numpy.int16)
        for s in range(self._n_samples):
            t = float(s) / self._sample_rate    # time in seconds

            # grab the x-coordinate of the sine wave at a given time,
            # constraining the sample to what our mixer is set to with "bits"
            wave_l = math.sin(2 * math.pi * self.frequency_l * t)
            wave_r = 0.5 * math.sin(2 * math.pi * self.frequency_r * t)
            buf[s][0] = int(round(self._max_sample * wave_l))  # left
            buf[s][1] = int(round(self._max_sample * wave_r))  # right

        return pygame.sndarray.make_sound(buf)


    def freq_are_close_enough(self):
        return math.fabs(self.frequency_l - self.frequency_r) < self._threshold

    def process_input(self, events):
        for e in events:
            # Don't touch any events that are not keyboard-related
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_UP:
                    self.frequency_r += self.FINE_STEP_HZ
                elif e.key == pygame.K_DOWN:
                    self.frequency_r -= self.FINE_STEP_HZ
                elif e.key == pygame.K_RIGHT:
                    self.frequency_r += self.COARSE_STEP_HZ
                elif e.key == pygame.K_LEFT:
                    self.frequency_r -= self.COARSE_STEP_HZ

    def debug_msg(self):
        return f'[DEBUG]: L: {self.frequency_l} Hz, R: {self.frequency_r}'


def main():
    size = (1366, 720)

    S = AudioSyncer()
    pygame.mixer.pre_init(44100, -S._bits, 2)
    pygame.init()
    _display_surf = pygame.display.set_mode(size, pygame.HWSURFACE | pygame.DOUBLEBUF)

    print("Use headphones!")

    _running = True
    while _running:
        sound = S.prepare_sound()
        sound.play()

        _events = []
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                print(S.debug_msg())
                close_enough = S.freq_are_close_enough()
                print("CLOSE ENOUGH" if close_enough else "TOO FAR")
                s = pygame.mixer.Sound(HYENA if close_enough else BOOM)
                s.play()
                _running = False
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_d:
                print(S.debug_msg())
            elif event.type == pygame.QUIT:
                _running = False
            else:
                _events.append(event)

        if _running:
            S.process_input(_events)

    pygame.quit()

if __name__ == "__main__":
    main()
