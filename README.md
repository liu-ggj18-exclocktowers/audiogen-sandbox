# Playground for audio-related things

Not supposed to be working out-of-the box and can eat your cat potentially. Use on your own risk.

I experiment using python3.6, so you should too.

## Installing

Use [virtualenv](http://www.pythonforbeginners.com/basics/how-to-use-python-virtualenv/), install [pygame](http://www.pygame.org/news), and make sure [pyaudio](http://people.csail.mit.edu/hubert/pyaudio/) is installed on your system using package manager, not in virtualenv

## Licensing

Hyena sound is taken from soundbible. [Description](http://soundbible.com/2191-Hyena-Laughing.html)
